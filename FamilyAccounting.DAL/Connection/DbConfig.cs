﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyAccounting.DAL.Connection
{
    public class DbConfig
    {
        public string ConnectionString { get; set; }
    }
}
