﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamilyAccounting.Web.Models
{
    public enum TransactionTypeViewModel
    {
            Income,
            Transfer,
            Expense,
            Initial
    }
}
