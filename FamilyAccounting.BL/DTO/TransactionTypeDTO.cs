﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FamilyAccounting.BL.DTO
{
    public enum TransactionTypeDTO
    {
        Income,
        Transfer,
        Expense
    }
    
}
